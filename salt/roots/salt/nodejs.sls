nodejs:
  pkg:
    - installed

# to map nodejs to node
alternative-nodejs:
  cmd.run:
    - name: update-alternatives --install /usr/bin/node node /usr/bin/nodejs 1
    - require:
      - pkg: nodejs
    - unless: test -L /usr/bin/node
